import React from "react";
import SampleComponent from "./components/sampleComponent/sampleComponent";
import "./styles/App.scss";

export default () => (
  <SampleComponent />
)