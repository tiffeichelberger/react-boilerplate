# React Boilerplate
Minimal React boilerplate set up for SCSS and GitLab Pages.

[![React Icon](https://reactjs.org/favicon-32x32.png)](https://reactjs.org/)  [![SASS Icon](https://sass-lang.com/favicon.ico)](https://sass-lang.com/)  [![Gitlab Pages Icon](https://docs.gitlab.com/favicon.ico?v=2)](https://sass-lang.com/)

## Clone project
```
git clone https://gitlab.com/tiffeichelberger/react-boilerplate
```

## Install dependencies
```
npm install
```

## Build project
```
npm run build
```

## Start development server
```
npm run start
```

[http://localhost:5000](http://localhost:5000) to view it locally.

[https://tiffeichelberger.gitlab.io/react-boilerplate/](https://tiffeichelberger.gitlab.io/react-boilerplate/) to view a demo.